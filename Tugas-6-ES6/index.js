//soal 1
const LuasPersegiPanjang = () => {
    let x = 10;
    let y = 10;
    luas = x * y;
    console.log("Luas Persegi Panjang = " +luas)
}
const KelilingPersegiPanjang = () => {
    let x = 10;
    let y = 10;
    keliling = 2 * (x + y)
    console.log("Keliling Persegi Panjang = " +keliling)
}

LuasPersegiPanjang()
KelilingPersegiPanjang()

//soal 2
const newFunction = (firstName, lastName) => {
    firstName
    lastName
    return {
      fullName(){
        return console.log(firstName + " " + lastName)
        }
    }
  }
newFunction("Naufal", "Daani").fullName()

//soal 3
let biodata = {
    firstName : "Naufal Daani",
    lastName : "Alhadi A",
    address : "Jalan Karang Anyar 2",
    hobby : "Doing positive things"
}
const {firstName, lastName, address, hobby} = biodata;

console.log(firstName, lastName, address, hobby);

//soal 4
const west = ['Will','Chris','Sam','Holly']
const east = ['Gill','Brian','Noel','Maggie']

let combined = [...west,...east]
console.log(combined)

//soal 5
const planet = "earth"
const view = "glass"
var after = "Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}"
console.log(after)

//note: untuk soal no 5 saya gak dapat eror-nya dimana mas, soalnya output beda, tapi code menurut saya sudah sesuai.