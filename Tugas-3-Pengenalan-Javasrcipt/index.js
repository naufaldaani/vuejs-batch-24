// Soal 1
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"
var proses1 = pertama.substr(0, 5)
var proses2 = pertama.substr(12, 7)
var proses3 = kedua.substr(0, 18)
var proses4 = proses1.concat(proses2)
var hasil = proses4.concat(proses3)
// console.log(hasil)

//soal 2
 var kataPertama = "10"
 var kataKedua = "2"
 var kataKetiga = "4"
 var kataKeempat = "6"
 var a = Number(kataPertama)
 var b = Number(kataKedua)
 var c = Number(kataKetiga)
 var d = Number(kataKeempat)
 var operasi = ((a + b) - c) * d; 
// console.log(operasi)
// console.log(operasi >= 24)
// console.log(operasi == 0)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(14, 4);  
var kataKetiga = kalimat.substring(18, 15); 
var kataKeempat = kalimat.substring(25, 19);  
var kataKelima = kalimat.substring(32, 25);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);